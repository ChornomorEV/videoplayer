package com.example.myvideoplayerapp.model;

import java.io.File;
import java.util.ArrayList;

public class DataManager {
    private static final DataManager instance = new DataManager();

    private File fileDirectory;
    private ArrayList<File> files = new ArrayList<>();

    private DataManager() {
        //Phone memory and SD card memory
        fileDirectory = new File("/mnt/");
    }

    public static DataManager getInstance() {
        return instance;
    }

    public ArrayList<File> getFiles() {
        if (files.isEmpty()) {
            scanFilesFromStorage(fileDirectory);
        }
        return files;
    }

    public File getFileByPosition(int position){
        return getFiles().get(position);
    }

    private void scanFilesFromStorage(File directory) {
        boolean fileExist = false;
        File[] listFile = directory.listFiles();
        if (listFile != null && listFile.length > 0) {
            for (File storageFile : listFile) {
                if (storageFile.isDirectory()) {
                    scanFilesFromStorage(storageFile);
                } else {
                    if (storageFile.getName().endsWith(".mp4")) {
                        for (File localFile : files) {
                            if (localFile.getName().equals(storageFile.getName())) {
                                fileExist = true;
                            }
                        }
                        if (fileExist) {
                            fileExist = false;
                        } else {
                            files.add(storageFile);
                        }
                    }
                }
            }
        }
    }
}
