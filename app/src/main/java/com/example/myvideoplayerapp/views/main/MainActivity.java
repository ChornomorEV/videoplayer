package com.example.myvideoplayerapp.views.main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.myvideoplayerapp.R;
import com.example.myvideoplayerapp.adapters.MyAdapter;
import com.example.myvideoplayerapp.presenters.MainPresenter;
import com.example.myvideoplayerapp.views.videoPlayer.VideoPlayerActivity;

import java.io.File;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends MvpAppCompatActivity implements MainView {

    @InjectPresenter MainPresenter mainPresenter;

    private RecyclerView myRecyclerView;
    public static int REQUEST_PERMISSION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myRecyclerView = findViewById(R.id.list_video_recycler);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        permissionForVideo();
    }

    private void permissionForVideo() {
        if ((ContextCompat.checkSelfPermission(getApplicationContext()
                , Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this
                    , Manifest.permission.READ_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}
                        , REQUEST_PERMISSION);
            }
        } else {
            mainPresenter.initFiles();
        }
    }

    @Override
    public void showFiles(ArrayList<File> files) {
        initAdapter(files);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mainPresenter.initFiles();
            }
        }
    }

    private void initAdapter(ArrayList<File> files) {
        MyAdapter myAdapter = new MyAdapter(files, new MyAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(int position) {
                openPlayerScreen(position);

            }
        });
        myRecyclerView.setAdapter(myAdapter);
    }

    private void openPlayerScreen(int position) {
        Intent intent = new Intent(this, VideoPlayerActivity.class);
        intent.putExtra("position", position);

        startActivity(intent);
    }
}

