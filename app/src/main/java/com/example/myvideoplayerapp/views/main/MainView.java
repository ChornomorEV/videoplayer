package com.example.myvideoplayerapp.views.main;

import com.arellomobile.mvp.MvpView;

import java.io.File;
import java.util.ArrayList;

public interface MainView extends MvpView {

    void showFiles(ArrayList<File> files);

}
