package com.example.myvideoplayerapp.views.videoPlayer;

import com.arellomobile.mvp.MvpView;

import java.io.File;

public interface VideoPlayerView extends MvpView {

    void showVideo(File file);
}
