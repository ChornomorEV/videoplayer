package com.example.myvideoplayerapp.views.videoPlayer;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.myvideoplayerapp.R;
import com.example.myvideoplayerapp.model.DataManager;
import com.example.myvideoplayerapp.presenters.VideoPlayerPresenter;

import java.io.File;

public class VideoPlayerActivity extends MvpAppCompatActivity implements View.OnClickListener, VideoPlayerView {

    @InjectPresenter VideoPlayerPresenter videoPlayerPresenter;

    private VideoView videoView;
    private int position;
    private TextView nameTrack;
    private Button btnBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        nameTrack = findViewById(R.id.tv_name_track);
        btnBack = findViewById(R.id.btn_return);
        btnBack.setOnClickListener(this);
        btnBack.setVisibility(View.VISIBLE);

        videoView = findViewById(R.id.video_view);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        position = getIntent().getIntExtra("position", -1);
        videoPlayerPresenter.findFileByPosition(position);
        nameTrack.setText(DataManager.getInstance().getFileByPosition(position).getName());

        getSupportActionBar().hide();
    }

    private void playVideo(File file) {

        MediaController mediaController = new MediaController(this);

        mediaController.setAnchorView(videoView);

        videoView.setMediaController(mediaController);
        videoView.setVideoPath(String.valueOf(file));
        videoView.requestFocus();

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                videoView.start();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_return:
                videoView.stopPlayback();
                onBackPressed();
        }
    }

    @Override
    public void showVideo(File file) {
        playVideo(file);
    }
}
