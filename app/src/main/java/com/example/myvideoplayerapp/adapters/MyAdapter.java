package com.example.myvideoplayerapp.adapters;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.myvideoplayerapp.R;

import java.io.File;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class MyAdapter extends RecyclerView.Adapter<VideoHolder> {

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    private ArrayList<File> videoArrayList;
    private OnItemClickListener listener;

    public MyAdapter(ArrayList<File> videoArrayList, OnItemClickListener listener) {
        this.videoArrayList = videoArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public VideoHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.video_item, viewGroup, false);
        return new VideoHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final VideoHolder holder, final int position) {

        holder.tvNameTrack.setText(videoArrayList.get(position).getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (videoArrayList.size() > 0) {
            return videoArrayList.size();
        } else {
            return 0;
        }
    }

}


class VideoHolder extends RecyclerView.ViewHolder {

    TextView tvNameTrack;

    public VideoHolder(View view) {
        super(view);

        tvNameTrack = view.findViewById(R.id.tv_name_track);
    }

}


