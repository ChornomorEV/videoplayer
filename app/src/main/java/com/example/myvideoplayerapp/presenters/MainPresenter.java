package com.example.myvideoplayerapp.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.myvideoplayerapp.model.DataManager;
import com.example.myvideoplayerapp.views.main.MainView;

import java.io.File;
import java.util.ArrayList;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    private ArrayList<File> files = new ArrayList<>();

    public void initFiles() {
        files = DataManager.getInstance().getFiles();
        getViewState().showFiles(files);
    }
}
