package com.example.myvideoplayerapp.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.myvideoplayerapp.model.DataManager;
import com.example.myvideoplayerapp.views.videoPlayer.VideoPlayerView;

import java.io.File;

@InjectViewState
public class VideoPlayerPresenter extends MvpPresenter<VideoPlayerView> {

    public void findFileByPosition(int position) {
        File file = DataManager.getInstance().getFileByPosition(position);
        getViewState().showVideo(file);
    }
}
